package view;

import javax.swing.JFrame;

import control.ColorButtonFrame;
import control.ColorRadioButtonFrame;

public class ColorRadioButtonViewer {
	public static void main(String[] args){
		JFrame frame = new ColorRadioButtonFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		}

}
