package view;

import javax.swing.JFrame;

import control.ColorComboBoxFrame;

public class ColorComboBoxViewer {
	public static void main(String[] args){
		JFrame frame = new ColorComboBoxFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}

}
