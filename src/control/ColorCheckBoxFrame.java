package control;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;

import control.ColorButtonFrame.ChoiceListener;

public class ColorCheckBoxFrame extends JFrame {
	

	private static final int FRAME_WIDTH = 300;
	private static final int FRAME_HEIGHT = 400;
	
	private JPanel colorPanel;
	private JCheckBox redCheckBox;
	private JCheckBox greenCheckBox;
	private JCheckBox blueCheckBox;
	
	public ColorCheckBoxFrame(){
		colorPanel = new JPanel();
		
		add(colorPanel, BorderLayout.CENTER);
		createControlPanel();
		setBackgroundColor();
		setSize(FRAME_WIDTH, FRAME_HEIGHT);
	}
	
	class ChoiceListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			 setBackgroundColor();
		}

	}
	
	public void createControlPanel(){
		ChoiceListener listener = new ChoiceListener();
		
		redCheckBox = new JCheckBox("Red");
		redCheckBox.addActionListener(listener);
		
		greenCheckBox = new JCheckBox("Green");
		greenCheckBox.addActionListener(listener);
		
		blueCheckBox = new JCheckBox("Blue");
		blueCheckBox.addActionListener(listener);
		
		JPanel colorPanel = new JPanel();
		colorPanel.setLayout(new FlowLayout());
		
		colorPanel.add(redCheckBox);
		colorPanel.add(greenCheckBox);
		colorPanel.add(blueCheckBox);
		
		add(colorPanel, BorderLayout.SOUTH);
	}
	
	public void setBackgroundColor(){
		if(redCheckBox.isSelected()){
			 colorPanel.setBackground(Color.RED);
		}
		else if(greenCheckBox.isSelected()){
			 colorPanel.setBackground(Color.GREEN);
		}
		else if(blueCheckBox.isSelected()){
			 colorPanel.setBackground(Color.BLUE);
		}
		colorPanel.repaint();
		
	}



}
