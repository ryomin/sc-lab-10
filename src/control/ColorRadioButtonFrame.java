package control;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import control.ColorButtonFrame.ChoiceListener;

public class ColorRadioButtonFrame extends JFrame {
	
	private static final int FRAME_WIDTH = 300;
	private static final int FRAME_HEIGHT = 400;
	
	private JPanel colorPanel;
	private JRadioButton redRButton;
	private JRadioButton greenRButton;
	private JRadioButton blueRButton;
	
	public ColorRadioButtonFrame(){
		colorPanel = new JPanel();
		
		add(colorPanel, BorderLayout.CENTER);
		createControlPanel();
		setBackgroundColor();
		setSize(FRAME_WIDTH, FRAME_HEIGHT);
	}
	
	class ChoiceListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			 setBackgroundColor();
		}

	}
	
	public void createControlPanel(){
		ChoiceListener listener = new ChoiceListener();
		
		redRButton = new JRadioButton("Red");
		redRButton.addActionListener(listener);
		
		greenRButton = new JRadioButton("Green");
		greenRButton.addActionListener(listener);
		
		blueRButton = new JRadioButton("Blue");
		blueRButton.addActionListener(listener);
		
		JPanel colorPanel = new JPanel();
		colorPanel.setLayout(new FlowLayout());
		
		colorPanel.add(redRButton);
		colorPanel.add(greenRButton);
		colorPanel.add(blueRButton);
		
		add(colorPanel, BorderLayout.SOUTH);
	}
	
	public void setBackgroundColor(){
		if(redRButton.isSelected()){
			 colorPanel.setBackground(Color.RED);
		}
		else if(greenRButton.isSelected()){
			 colorPanel.setBackground(Color.GREEN);
		}
		else if(blueRButton.isSelected()){
			 colorPanel.setBackground(Color.BLUE);
		}
		colorPanel.repaint();
		
	}



}
