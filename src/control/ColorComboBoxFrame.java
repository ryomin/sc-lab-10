package control;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;

import control.ColorButtonFrame.ChoiceListener;

public class ColorComboBoxFrame extends JFrame {
	
	private static final int FRAME_WIDTH = 300;
	private static final int FRAME_HEIGHT = 400;
	
	private JPanel colorPanel;
	private JComboBox colorComboBox;
	
	public ColorComboBoxFrame(){
		colorPanel = new JPanel();
		
		add(colorPanel, BorderLayout.CENTER);
		createControlPanel();
		setBackgroundColor();
		setSize(FRAME_WIDTH, FRAME_HEIGHT);
	}
	
	class ChoiceListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			if (colorComboBox.getSelectedItem().toString().equals("Red")){
				colorPanel.setBackground(Color.RED);
			}
			if (colorComboBox.getSelectedItem().toString().equals("Green")){
				colorPanel.setBackground(Color.BLUE);
			}
			if (colorComboBox.getSelectedItem().toString().equals("Blue")){
				colorPanel.setBackground(Color.GREEN);
			}
	
			 setBackgroundColor();
		}

	}
	
	public void createControlPanel(){
		ChoiceListener listener = new ChoiceListener();
		colorComboBox.addItem("Red");
		colorComboBox.addItem("Green");
		colorComboBox.addItem("Blue");
		colorComboBox.setEditable(true);
		colorComboBox.addActionListener(listener);
		colorPanel.add(colorComboBox);
		
		add(colorPanel, BorderLayout.SOUTH);
	}
	
	public void setBackgroundColor(){
		colorPanel.repaint();
		
	}

}
