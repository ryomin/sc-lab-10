package control;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeListener;


public class ColorButtonFrame extends JFrame{
	
	private static final int FRAME_WIDTH = 300;
	private static final int FRAME_HEIGHT = 400;
	
	private JPanel colorPanel;
	private JButton redButton;
	private JButton greenButton;
	private JButton blueButton;
	
	public ColorButtonFrame(){
		colorPanel = new JPanel();
		
		add(colorPanel, BorderLayout.CENTER);
		createControlPanel();
		setBackgroundColor();
		setSize(FRAME_WIDTH, FRAME_HEIGHT);
	}
	
	class ChoiceListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			 if (event.getActionCommand().equals("Red")) {
				 colorPanel.setBackground(Color.RED);
			  }
			 else if(event.getActionCommand().equals("Green")) {
				 colorPanel.setBackground(Color.GREEN);
			  } 
			 else if(event.getActionCommand().equals("Blue")) {
				 colorPanel.setBackground(Color.BLUE);
			  } 
			 setBackgroundColor();
		}

	}
	
	public void createControlPanel(){
		ChoiceListener listener = new ChoiceListener();
		
		redButton = new JButton("Red");
		redButton.addActionListener(listener);
		
		greenButton = new JButton("Green");
		greenButton.addActionListener(listener);
		
		blueButton = new JButton("Blue");
		blueButton.addActionListener(listener);
		
		JPanel colorPanel = new JPanel();
		colorPanel.setLayout(new FlowLayout());
		
		colorPanel.add(redButton);
		colorPanel.add(greenButton);
		colorPanel.add(blueButton);
		
		add(colorPanel, BorderLayout.SOUTH);
	}
	
	public void setBackgroundColor(){
		colorPanel.repaint();
		
	}


}
